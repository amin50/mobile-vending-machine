class Snack {
  final int id;
  final String name;
  final int price;
  final int totalStuff;

  const Snack({
    required this.id,
    required this.name,
    required this.price,
    required this.totalStuff,
  });

  factory Snack.fromJson(Map<String, dynamic> json) {
    return Snack(
      id: json['id'],
      name: json['name'],
      price: json['price'],
      totalStuff: json['totalStuff'],
    );
  }
}