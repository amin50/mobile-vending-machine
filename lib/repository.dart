import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mobile_vending_machine/model.dart';

class Repository {
  // final _baseUrl = 'https://jsonplaceholder.typicode.com/posts/1/comments';
  final _baseUrl = 'http://192.168.1.8/VendingMachine/api/Transaction';
  // final _chooseUrl = '/chooseMenu';
  // final _chooseUrl = 'http://192.168.1.8/VendingMachine/api/Transaction/chooseMenu?caseNumber=1';

  Future getData() async {
    try {
      final response = await http.get(Uri.parse(_baseUrl));

      if (response.statusCode == 200) {
        // print(response.body);
        Iterable it = jsonDecode(response.body);
        List<Snack> snacks = it.map((e) => Snack.fromJson(e)).toList();
        return snacks;
      }
    } catch (e) {
      print(e.toString());
    }
  }

  Future chooseMenu(int id) async {
    try {
      print(id);

      String _chooseUrl =
          'http://192.168.1.8/VendingMachine/api/Transaction/chooseMenu?caseNumber=$id';

      final response = await http.get(Uri.parse(_chooseUrl));
      Snack? snack;
      if (response.statusCode == 200) {
        var decode = jsonDecode(response.body);
        snack = Snack.fromJson(decode);
      } else {
        snack = null;
      }
      return snack;
    } catch (e) {
      print(e.toString());
    }
  }

  Future inputMoney(int choosenSnackId, int inputPrice) async {
    try {

      String _inputMoneyUrl =
          'http://192.168.1.8/VendingMachine/api/Transaction/inputMoney?choosenSnackId=$choosenSnackId&inputPrice=$inputPrice';

      final response = await http.get(Uri.parse(_inputMoneyUrl));
      String messageResponse = "";

      if (response.statusCode == 200) {
        var decode = jsonDecode(response.body);
        messageResponse = decode["message"].toString();
        // print(decode["message"].toString());
        // print(messageResponse);
      }
      return messageResponse;
    } catch (e) {
      print(e.toString());
    }
  }
}
