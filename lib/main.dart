import 'package:flutter/material.dart';
import 'package:mobile_vending_machine/model.dart';
import 'package:mobile_vending_machine/repository.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Vending Machine',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Vending Machine Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Snack> snacks = [];
  // Snack? snack;
  Repository repository = Repository();
  TextEditingController nameController = TextEditingController();

  getData() async {
    snacks = await repository.getData();
    setState(() {});
  }

  testInputMoney() async {
    await repository.inputMoney(1, 10000);
  }

  @override
  void initState() {
    getData();
    // testInputMoney();
    super.initState();
  }

  String label = "Masukan nomor untuk memilih.";
  String hintText = "Input nomor pilihan";
  String choosenValue = "Choosen Value";
  bool isChooseMenu = false;
  int? snackId = 0;

  void _executeChooseMenu() async {
    String inputValue = nameController.text;

    Snack? snack = await repository.chooseMenu(int.parse(inputValue));
    snackId = snack?.id;

    if (snack != null) {
      String id = snack.id.toString();
      String name = snack.name;
      String price = snack.price.toString();

      choosenValue = '$id . $name $price';
      setState(() {});
      nameController.clear();
      label = 'Masukan uang anda! *(2000, 5000, 10000, 20000, 50000)';
      hintText = 'Input uang';
      isChooseMenu = true;
    } else {
      choosenValue = 'Nomor yang dipilih tidak ada di menu';
      setState(() {});
    }
  }

  void _executeInputMoney() async {
    String inputValue = nameController.text;
    int inputMoneyValue = int.parse(inputValue);
    print('ID SNACK => $snackId');
    print('INPUT MONEY VALUE => $inputMoneyValue');

    String inputMoney =
        await repository.inputMoney(snackId as int, inputMoneyValue);

    print('INPUT MONEY RESULT => $inputMoney');
    choosenValue = inputMoney;
    setState(() {});
    nameController.clear();
    label = '';
    hintText = '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      // drawer: RmoMenu(),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: snacks.length,
              itemBuilder: (BuildContext context, int index) {
                String id = snacks[index].id.toString();
                String name = snacks[index].name;
                String price = snacks[index].price.toString();
                return Text('$id . $name $price');
              },
            ),
          ),
          Text(choosenValue),
          Text(label),
          TextField(
            controller: nameController,
            decoration: InputDecoration(
              border: const OutlineInputBorder(),
              hintText: hintText,
            ),
          )
          // Text('Nomor Pilihan'),

          // Text('Masukan uang anda! *(2000, 5000, 10000, 20000, 50000)'),
          // Text('Uang Anda'),

          // Text('Masukan uang anda! *(2000, 5000, 10000, 20000, 50000)'),
          // Text('Uang Anda'),
        ],
      ),

      floatingActionButton: FloatingActionButton(
        // onPressed: _execute,
        onPressed: () => {
          if (!isChooseMenu)
            {_executeChooseMenu()}
          else if (isChooseMenu)
            {_executeInputMoney()}
        },

        tooltip: 'Increment',
        child: const Icon(Icons.check),
      ), // This trailing comma makes auto-formatting nicer for build methods.

      // onWillPop: () => logoutAlert(context: context),
    );
  }
}
